diff --git a/BUILD.gn b/BUILD.gn
index 7db59621..cba8d774 100644
--- a/BUILD.gn
+++ b/BUILD.gn
@@ -206,6 +206,8 @@ static_library("libupdate_engine") {
     "cros/requisition_util.cc",
     "cros/shill_proxy.cc",
     "cros/update_attempter.cc",
+    "cros/fydeos_license_checker.cc",
+    "cros/fydeos_toggle_ota.cc",
     "libcurl_http_fetcher.cc",
     "metrics_utils.cc",
     "update_boot_flags_action.cc",
diff --git a/cros/daemon_chromeos.cc b/cros/daemon_chromeos.cc
index 366fb9a9..f18e5d19 100644
--- a/cros/daemon_chromeos.cc
+++ b/cros/daemon_chromeos.cc
@@ -37,6 +37,10 @@ int DaemonChromeOS::OnInit() {
   // handler.
   subprocess_.Init(this);
 
+  // ---***FYDEOS BEGIN***---
+  license_checker_.Init();
+  // ---***FYDEOS END***---
+
   int exit_code = Daemon::OnInit();
   if (exit_code != EX_OK)
     return exit_code;
diff --git a/cros/daemon_chromeos.h b/cros/daemon_chromeos.h
index b23c2a6f..7468372b 100644
--- a/cros/daemon_chromeos.h
+++ b/cros/daemon_chromeos.h
@@ -24,6 +24,9 @@
 #include "update_engine/common/subprocess.h"
 #include "update_engine/cros/dbus_service.h"
 #include "update_engine/cros/real_system_state.h"
+// ---***FYDEOS BEGIN***---
+#include "update_engine/cros/fydeos_license_checker.h"
+// ---***FYDEOS END***---
 
 namespace chromeos_update_engine {
 
@@ -55,6 +58,10 @@ class DaemonChromeOS : public DaemonBase {
   // the main() function.
   Subprocess subprocess_;
 
+  // ---***FYDEOS BEGIN***---
+  fydeos::FydeLicenseChecker license_checker_;
+  // ---***FYDEOS END***---
+
   DISALLOW_COPY_AND_ASSIGN(DaemonChromeOS);
 };
 
diff --git a/cros/fydeos_license_checker.cc b/cros/fydeos_license_checker.cc
new file mode 100644
index 00000000..25eb7321
--- /dev/null
+++ b/cros/fydeos_license_checker.cc
@@ -0,0 +1,46 @@
+#include "update_engine/cros/fydeos_license_checker.h"
+
+#include <base/logging.h>
+#include <base/strings/stringprintf.h>
+#include <base/optional.h>
+#include <base/json/json_reader.h>
+#include <base/base64.h>
+#include <base/values.h>
+#include <crypto/signature_verifier.h>
+
+#include "update_engine/common/subprocess.h"
+
+namespace fydeos {
+
+namespace {
+  const char kShellCmd[] = "/usr/share/fydeos_shell/license-utils.sh";
+  const char kParamId[] = "id";
+} // namespace
+
+FydeLicenseChecker::FydeLicenseChecker() = default;
+FydeLicenseChecker::~FydeLicenseChecker() {
+  if (license_checker_singleton_ == this) {
+    license_checker_singleton_ = nullptr;
+  }
+}
+
+void FydeLicenseChecker::Init() {
+  if (license_checker_singleton_ == this)
+    return;
+  CHECK(license_checker_singleton_ == nullptr);
+  license_checker_singleton_ = this;
+
+  int exit_code = 0;
+  std::string value, error;
+  std::vector<std::string> cmd = {kShellCmd, kParamId};
+  if (!chromeos_update_engine::Subprocess::SynchronousExec(cmd, &exit_code, &value, &error) || exit_code) {
+    LOG(ERROR) << "Get fydeos license id error, exit_code: " << exit_code;
+    return;
+  }
+  base::TrimWhitespaceASCII(value, base::TRIM_ALL, &license_id_);
+  LOG(INFO) << "FydeOS license id: " << license_id_;
+}
+
+FydeLicenseChecker* FydeLicenseChecker::license_checker_singleton_ = nullptr;
+
+} // namespace fydeos
diff --git a/cros/fydeos_license_checker.h b/cros/fydeos_license_checker.h
new file mode 100644
index 00000000..110e47fe
--- /dev/null
+++ b/cros/fydeos_license_checker.h
@@ -0,0 +1,25 @@
+#ifndef UPDATE_ENGINE_FYDEOS_LICENSE_CHECKER_H
+#define UPDATE_ENGINE_FYDEOS_LICENSE_CHECKER_H
+
+#include <string>
+#include <memory>
+
+namespace fydeos {
+
+class FydeLicenseChecker {
+  public:
+    explicit FydeLicenseChecker();
+    ~FydeLicenseChecker();
+
+    static FydeLicenseChecker& Get() { return *license_checker_singleton_; }
+    void Init();
+    std::string GetLicenseId() const { return license_id_; };
+
+  private:
+    static FydeLicenseChecker* license_checker_singleton_;
+
+    std::string license_id_;
+};
+
+} // namespace fydeos
+#endif /* ifndef UPDATE_ENGINE_FYDEOS_LICENSE_CHECKER_H */
diff --git a/cros/fydeos_toggle_ota.cc b/cros/fydeos_toggle_ota.cc
new file mode 100644
index 00000000..c0397652
--- /dev/null
+++ b/cros/fydeos_toggle_ota.cc
@@ -0,0 +1,15 @@
+#include "update_engine/cros/fydeos_toggle_ota.h"
+#include <base/files/file_util.h>
+
+namespace fydeos {
+namespace {
+  // keep it the same with chromium src fydeos/misc/fydeos_toggle_ota.cc
+  const char kFydeOSOTAIndicatorFile[] = "/mnt/stateful_partition/unencrypted/preserve/.disable_fydeos_ota";
+}
+
+bool OTAEnabled() {
+  const bool enabled = !base::PathExists(base::FilePath(kFydeOSOTAIndicatorFile));
+  return enabled;
+}
+
+} // fydeos
diff --git a/cros/fydeos_toggle_ota.h b/cros/fydeos_toggle_ota.h
new file mode 100644
index 00000000..2c0fbb05
--- /dev/null
+++ b/cros/fydeos_toggle_ota.h
@@ -0,0 +1,8 @@
+#ifndef FYDEOS_TOGGLE_OTA_H_
+#define FYDEOS_TOGGLE_OTA_H_
+
+namespace fydeos {
+  bool OTAEnabled();
+}
+
+#endif // ifndef FYDEOS_TOGGLE_OTA_H_
diff --git a/cros/omaha_request_action.cc b/cros/omaha_request_action.cc
index e59ba01c..2588873a 100644
--- a/cros/omaha_request_action.cc
+++ b/cros/omaha_request_action.cc
@@ -54,6 +54,9 @@
 #include "update_engine/cros/payload_state_interface.h"
 #include "update_engine/cros/update_attempter.h"
 #include "update_engine/metrics_utils.h"
+// ---***FYDEOS BEGIN***---
+#include "update_engine/cros/fydeos_toggle_ota.h"
+// ---***FYDEOS END***---
 
 using base::Time;
 using base::TimeDelta;
@@ -1270,6 +1273,11 @@ bool OmahaRequestAction::ShouldIgnoreUpdate(ErrorCode* error) const {
     LOG(ERROR) << "All packages were excluded.";
   }
 
+  if (!fydeos::OTAEnabled()) {
+    LOG(INFO) << "fydeos ota disabled, ignore update";
+    return true;
+  }
+
   // Note: We could technically delete the UpdateFirstSeenAt state when we
   // return true. If we do, it'll mean a device has to restart the
   // UpdateFirstSeenAt and thus help scattering take effect when the AU is
diff --git a/cros/omaha_request_builder_xml.cc b/cros/omaha_request_builder_xml.cc
index 590e84ef..3a89548d 100644
--- a/cros/omaha_request_builder_xml.cc
+++ b/cros/omaha_request_builder_xml.cc
@@ -33,6 +33,9 @@
 #include "update_engine/common/utils.h"
 #include "update_engine/cros/omaha_request_params.h"
 #include "update_engine/cros/update_attempter.h"
+// ---***FYDEOS BEGIN***---
+#include "update_engine/cros/fydeos_license_checker.h"
+// ---***FYDEOS END***---
 
 using std::string;
 
@@ -293,6 +296,11 @@ string OmahaRequestBuilderXml::GetApp(const OmahaAppData& app_data) const {
     app_versions = "version=\"" +
                    XmlEncodeWithDefault(app_data.version, kNoVersion) + "\" ";
   }
+  // ---***FYDEOS BEGIN***---
+  string license_id = fydeos::FydeLicenseChecker::Get().GetLicenseId();
+  string fydeos_license_id =
+      "fydeos_license_id=\"" + XmlEncodeWithDefault(license_id) + "\" ";
+  // ---***FYDEOS END***---
 
   string download_channel = params->download_channel();
   string app_channels =
@@ -403,6 +411,9 @@ string OmahaRequestBuilderXml::GetApp(const OmahaAppData& app_data) const {
       "lang=\"" + XmlEncodeWithDefault(params->app_lang(), "en-US") + "\" " +
       requisition_arg) +
 
+      // ---***FYDEOS BEGIN***---
+      fydeos_license_id +
+      // ---***FYDEOS END***---
       ">\n" +
          app_body +
       "    </app>\n";
diff --git a/cros/update_attempter.cc b/cros/update_attempter.cc
index 197f7a1d..5e12e501 100644
--- a/cros/update_attempter.cc
+++ b/cros/update_attempter.cc
@@ -71,6 +71,9 @@
 #include "update_engine/update_manager/omaha_request_params_policy.h"
 #include "update_engine/update_manager/update_manager.h"
 #include "update_engine/update_status_utils.h"
+// ---***FYDEOS BEGIN***---
+#include "update_engine/cros/fydeos_toggle_ota.h"
+// ---***FYDEOS END***---
 
 using base::Bind;
 using base::Callback;
@@ -175,6 +178,13 @@ bool UpdateAttempter::ScheduleUpdates() {
   if (IsBusyOrUpdateScheduled())
     return false;
 
+  // ---***FYDEOS BEGIN***---
+  if (!fydeos::OTAEnabled()) {
+    LOG(INFO) << "fydeos ota disabled, skip scheduling updates";
+    return false;
+  }
+  // ---***FYDEOS END***---
+
   // We limit the async policy request to a reasonably short time, to avoid a
   // starvation due to a transient bug.
   policy_data_.reset(new UpdateCheckAllowedPolicyData());
@@ -951,6 +961,13 @@ bool UpdateAttempter::CheckForUpdate(const string& app_version,
               << " already in progress.";
     return false;
   }
+  // ---***FYDEOS BEGIN***---
+  if (!fydeos::OTAEnabled()) {
+    LOG(INFO) << "fydeos ota disabled, refuse to check for update";
+    BroadcastStatus();
+    return false;
+  }
+  // ---***FYDEOS END***---
 
   bool interactive = !(flags & UpdateAttemptFlags::kFlagNonInteractive);
   is_install_ = false;
diff --git a/update_manager/official_build_check_policy_impl.cc b/update_manager/official_build_check_policy_impl.cc
index 59730684..3c621da3 100644
--- a/update_manager/official_build_check_policy_impl.cc
+++ b/update_manager/official_build_check_policy_impl.cc
@@ -24,6 +24,8 @@ EvalStatus OnlyUpdateOfficialBuildsPolicyImpl::Evaluate(
     State* state,
     std::string* error,
     PolicyDataInterface* data) const {
+// ---***FYDEOS BEGIN***---
+  /*
   const bool* is_official_build_p =
       ec->GetValue(state->system_provider()->var_is_official_build());
   if (is_official_build_p != nullptr && !(*is_official_build_p)) {
@@ -38,6 +40,8 @@ EvalStatus OnlyUpdateOfficialBuildsPolicyImpl::Evaluate(
     LOG(INFO) << "Unofficial build, but periodic update check interval "
               << "timeout is defined, so update is not blocked.";
   }
+  */
+// ---***FYDEOS END***---
   return EvalStatus::kContinue;
 }
 
